/*
* fichier : main.cpp
* auteur : Marie-Helene Gadbois-Del Carpio - 19091796
* auteur : Pierre-Daniel Godfrey - 17179777
* date :
* description : programme qui affiche un histogramme
*/
#include <iostream>
#include <climits>
#include <vector>
#include <cmath>

using namespace std;


int main()
{
	// fonctions utilisees
	vector <int> lireValeurs(int, int&, int&);
	vector <int> classerValeurs(int,int,int, const vector <int>);
	void afficherHisto(const vector <int>);

	//donn�es locales
	int nbCategories;             //le nombre de categories
	int nbValeurs;                //Le nombre de valeurs
	vector <int> valeurs;         //le vecteur contenant toute les valeurs
	vector <int> categories;      // Le vecteur contenant les differentes categories
	int minimum;                  //La valeur minimale
	int maximum;                  //La valeur maximale


	//histogramme
	cout << "Entrez le nombre de categories" << endl;
	cin >> nbCategories;
	cout << "Entrez le nombre de valeurs" << endl;
	cin >> nbValeurs;
    valeurs = lireValeurs(nbValeurs, minimum, maximum);
	categories = classerValeurs(minimum,maximum,nbCategories,valeurs);
	afficherHisto(categories);
}

vector<int> lireValeurs(int nbValeurs, int & min, int & max)
{
	min = INT_MAX;
	max = INT_MIN;
	vector <int> valeurs;

	cout << "Entrez les valeurs" << endl;
	for (int nbValeursLues = 0; nbValeursLues < nbValeurs; nbValeursLues++)
	{
		int valeur;
		cin >> valeur;
		valeurs.push_back(valeur);

		if (valeur < min)
		{
			min = valeur;
		}
		if (valeur> max)
		{
			max = valeur;
		}
	}
	return valeurs;
}

 vector<int>classerValeurs(int min,int max,int nbCategories, vector <int> valeurs)
{
	int position;                                                 //La position dans le vecteur valeurs
	int intervalle = max - min+1;                                 //Calculer l'intervalle
	double tailleInter = 1.0 * intervalle / nbCategories;         //Calculer la taille de l'intervalle
	vector<int>categories;                                        //Le vecteur des cat�gories
	int categorie = 1;                                            //initialiser le num�ro de cat�gorie � 1
	int nbElementsCategorie;                                      //La quantit� d'�l�ments dans une cat�gorie
	float borneSuperieure;                                        //Le nombre � gauche de l'intervalle 
	float borneInferieure;                                        //Le nombre � droite de l'intervalle

	
	while(categories.size() <= nbCategories)
	{
		borneSuperieure = min + tailleInter * categorie;
		borneInferieure = min + tailleInter * (categorie - 1);
		nbElementsCategorie = 0;
		for (position = 0; position < valeurs.size(); position++)
		{
			if (valeurs.at(position) >= borneInferieure && valeurs.at(position) < borneSuperieure)
			{
				nbElementsCategorie++;
			}
		}
		categories.push_back(nbElementsCategorie);
		categorie++;
	}
	
	return categories;
 }

void afficherHisto(vector <int> categories)
{
	int nbEtoiles;
	for (int element=0; element< categories.size(); element++)
	{
		nbEtoiles = 0;
		while (nbEtoiles < categories.at(element))
		{
			cout << "*";
			nbEtoiles++;
		}
		cout << endl;
	}

}
